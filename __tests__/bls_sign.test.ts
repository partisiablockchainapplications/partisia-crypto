it('all below is deprecated', () => {})

// import { partisiaCrypto } from '../src/index'
// import { Payload_NodeRegisterSynaps } from '../src/main/abi_system'
// import { serializeToBuffer } from '../src/main/structs'
// import { convertSignatureEthereumToPartisia, getSynapsMessageHash } from '../src/main/contract_system'
// import { bufferToNum, createHashSha256, numToBuffer } from '../src/main/utils-buffer'
// import { privateBlsToPublic, sign } from '../src/main/sign_bls'
// import { privateKeyToKeypair } from '../src/main/wallet'

// it('derive signing message for bls syanps', () => {
//     // Test values
//     const keyBls = numToBuffer(321, 32 * 8).reverse()
//     const hexBls = typeof keyBls === 'string' ? keyBls : keyBls.toString('hex')

//     const keyEc = numToBuffer(123, 32 * 8).reverse()
//     const publicKey = Buffer.from(privateKeyToKeypair(keyEc.toString('hex')).getPublic(true, 'array'));

//     const blsPublicKey = privateBlsToPublic(hexBls);
//     const address = '00' + createHashSha256(Buffer.concat([numToBuffer(12, 32).reverse(), Buffer.from("test address", 'utf8')])).toString('hex').substring(24)
//     const sessionId = 1

//     const hash = getSynapsMessageHash(address, publicKey, blsPublicKey, sessionId)
//     expect(hash.toString('hex')).toBe('00dc0bd68e0a030027c07bfe388af26794fad3b614b4aaa144e521abf1de481f')
// })

// it('sign node register synaps', () => {
//     // testing for Synaps KYC/B contract
//     // Synaps KYC / B contract
//     // https://gitlab.com/secata/pbc/governance/synaps-kycb-contract/-/blob/par_4334_smart_contract/src/main/java/com/partisiablockchain/governance/SynapsKycbContract.java

//     // const data = {
//     //   "result": {
//     //     "message": "{\"corporate_id\":1,\"name\":\"PBC\",\"city\":\"Aarhus\",\"country\":\"DNK\",\"country_numeric\":\"208\",\"address\":\"Aabogade 15\",\"registration_number\":\"1234567890\",\"provider\":\"Synaps\",\"partisia_blockchain_address\":\"kristian.hu@secata.com\"}",
//     //     "signature": "4ed23083d1469e2bf4c1aabeb0732a7e7217013e7c854aa72eeed974cd8d4d94030f97f5b20b32c994b41a7d5d8fffd9352a1830005ae63b9a172c42ab04ff0001"
//     //   }
//     // }

//     // const publicKeyBls = privateBlsToPublic(utils_buffer.numToBuffer(444, 64).toString('hex'))
//     const dataRpc = { "corporate_id": 1, "name": "WKL Holding SAS", "city": "Paris", "country": "FRA", "country_numeric": "250", "address": "35 Guy moquet Street", "registration_number": "901215020", "provider": "Synaps", "partisia_blockchain_address": "0073f36afe58f3c929933519b6cf8fd4edd914e759" }
//     const dataRpcBuf = '7b22636f72706f726174655f6964223a312c226e616d65223a22574b4c20486f6c64696e6720534153222c2263697479223a225061726973222c22636f756e747279223a22465241222c22636f756e7472795f6e756d65726963223a22323530222c2261646472657373223a22333520477579206d6f7175657420537472656574222c22726567697374726174696f6e5f6e756d626572223a22393031323135303230222c2270726f7669646572223a2253796e617073222c2270617274697369615f626c6f636b636861696e5f61646472657373223a22303037336633366166653538663363393239393333353139623663663866643465646439313465373539227d'
//     const dataSigEth = '172d7d5e5f84bfa463afc693b180d12196f43580f564a90c273b3bc9e3186f7f3564109e7e74e2058a307f6134c5d6e265e9066f8d59c9a906cf6d417bd0766500'

//     const keyBls = numToBuffer(444, 32 * 8).reverse()

//     // derive the keys from secret
//     const hexBls = typeof keyBls === 'string' ? keyBls : keyBls.toString('hex')
//     const publicKeyBls = privateBlsToPublic(hexBls)


//     const publicKeyBuf = Buffer.from('A6WYqAMNpthsa8fy9RROpUnSghHqWPqnDr9MHmZcH+m1', 'base64')

//     // build the message to bls sign
//     // const message = Buffer.concat([Buffer.from(address, 'hex'), publicKeyBuf, publicKeyBls, numToBuffer(1, 64).reverse()])
//     // liturally the string 'pop'
//     const hashPop = createHashSha256(Buffer.concat([numToBuffer(3, 32).reverse(), Buffer.from('pop', 'utf8')]))

//     // sign it with bls
//     const pk_to_num = bufferToNum(Buffer.from(hexBls, 'hex'))
//     const sk = BigInt(pk_to_num)
//     const signatureBls = sign(sk, hashPop)

//     const obj = {
//         invocationByte: 1,
//         rawRequest: JSON.stringify(dataRpc), // '{"corporate_id":1,"name":"WKL Holding SAS","city":"Paris","country":"FRA","country_numeric":"250","address":"35 Guy moquet Street","registration_number":"901215020","provider":"Synaps","partisia_blockchain_address":"0073f36afe58f3c929933519b6cf8fd4edd914e759"}',
//         signatureOnRawRequest: convertSignatureEthereumToPartisia(dataSigEth).toString('hex'),
//         website: 'http://localhost',
//         producerServerJurisdiction: 123,
//         producerPublicKey: publicKeyBuf.toString('hex'),
//         producerBlsKey: publicKeyBls.toString('hex'),
//         popSignature: signatureBls.toString('hex'),
//     }

//     const abi = Payload_NodeRegisterSynaps
//     const trx = serializeToBuffer(obj, ...abi)
//     // where
//     // - Signature@65377d8f is the converted signature from the eth signature 172d7d5e5f84bfa463afc693b180d12196f43580f564a90c273b3bc9e3186f7f3564109e7e74e2058a307f6134c5d6e265e9066f8d59c9a906cf6d417bd0766500
//     // - producerPublicKey is created as new KeyPair(BigInteger.valueOf(123)).getPublic()
//     // - producerBlsKey and popSignature is created as

//     // BlsKeyPair blsKeyPair = new BlsKeyPair(BigInteger.valueOf(444));
//     // producerBlsKey = blsKeyPair.getPublicKey();
//     // popSignature = blsKeyPair.sign(Hash.create(s -> s.writeString("pop")));


//     expect(trx.toString('hex')).toBe('01000001047b22636f72706f726174655f6964223a312c226e616d65223a22574b4c20486f6c64696e6720534153222c2263697479223a225061726973222c22636f756e747279223a22465241222c22636f756e7472795f6e756d65726963223a22323530222c2261646472657373223a22333520477579206d6f7175657420537472656574222c22726567697374726174696f6e5f6e756d626572223a22393031323135303230222c2270726f7669646572223a2253796e617073222c2270617274697369615f626c6f636b636861696e5f61646472657373223a22303037336633366166653538663363393239393333353139623663663866643465646439313465373539227d00172d7d5e5f84bfa463afc693b180d12196f43580f564a90c273b3bc9e3186f7f3564109e7e74e2058a307f6134c5d6e265e9066f8d59c9a906cf6d417bd0766500000010687474703a2f2f6c6f63616c686f73740000007b03a598a8030da6d86c6bc7f2f5144ea549d28211ea58faa70ebf4c1e665c1fe9b58e8d48014cf3c9511ec252128435f3be5b4dc1e555c0bcf9f5738056a36045ca860c85033d21c199140303aabcd1627503e53fb4dc8adcdcc9d852b23c40d10c47766138c14c25d525f701acb076b850ace1215f9783894db446f4fc5b2054ffa7cf92e419cd743a5e8114f00350d9dad8d70b51c3aab02ec593aaaca58c9be17820d296bcae7400fecaf4ac98ab7715')
// })
// // it('sign node register', () => {
// //     // required two signatures; transaction and bls

// //     // > New random key pair:
// //     const objKey = {
// //         PrivateKey: 'f544b67ad90eba40308804f2ebb67b25b8f9ed45bf29d58c19b3092013277b29',
// //         BlockchainAddress: '0073bd2b5f347fd3bcbc1b424a9f9fc97186624778',
// //         PublicKey: '0303aeecdd6382589bffe2461422663e8aa0a82e49b691bf866795ebdf5b7faa3b',
// //     }
// //     // > New random producer key (BLS12-381):
// //     const objBls = {
// //         PrivateKey: '4d0ca975239288b7f818f0ad714f5cb1d8e37a9201a80a2fc77b37300ea8024e',
// //         PublicKey: 'b83248c93fa466579456d246722d58d7e980e611da3a3b572d712f9b0768f9d5b3b1d5a51f7f8856c3892728e9972f67043f1e0364a604281d239bc22507e4ce3b822a11096cc135792eb17d75630a2c7bce7c27ce0b40439f31333908d4dc50',
// //     }
// //     const x = partisiaCrypto.contract_system.Node.nodeRegister({
// //         name: "test",
// //         website: "xyz.com",
// //         streetAddress: "Baker Street 22, xx US",
// //         jurisdictionEntity: 123,
// //         jurisdictionServer: 122,
// //     }, '0073bd2b5f347fd3bcbc1b424a9f9fc97186624778', objKey.PublicKey, objBls.PrivateKey)
// //     console.log('x', x.toString('hex'))
// //     expect(x.toString('hex')).toEqual('0000000004746573740000000778797a2e636f6d0000001642616b6572205374726565742032322c2078782055530303aeecdd6382589bffe2461422663e8aa0a82e49b691bf866795ebdf5b7faa3bb83248c93fa466579456d246722d58d7e980e611da3a3b572d712f9b0768f9d5b3b1d5a51f7f8856c3892728e9972f67043f1e0364a604281d239bc22507e4ce3b822a11096cc135792eb17d75630a2c7bce7c27ce0b40439f31333908d4dc500000007b0000007aad76adb7988831fb55de92d755e7a87bf106b2c1787c1ed50e80091687acb5972c320d936d5d33072e209507fcb78606')
// // })