import { partisiaCrypto } from '../src/index'

it('withdraw epoch', () => {
    const withdrawState = {
        "byocContract": {
            "identifier": {
                "data": "qGXkESbXdwsSow3Htmt0QCTA86Q="
            }
        },
        "currentEpoch": "1",
        "epochs": [
            {
                "key": "0",
                "value": {
                    "disputes": [],
                    "merkleTree": "c116e838ce4434b710ce3f5b218d4b41fb338ad3164d23f0c08a1271f86f3c4f",
                    "oracles": [
                        {
                            "identity": "0035e97a5e078a5a0f28ec96d547bfee9ace803ac0",
                            "key": "A7NVEdZ+Y/plUtt0C0irptIwwheZ5lpmR6XL/Hie8BhL"
                        },
                        {
                            "identity": "00b2e734b5d8da089318d0d2b076c19f59c450855a",
                            "key": "A54hWPDXwNXybDeR7++nlZdlTnorJGT1Kx7mwTR3ae9X"
                        },
                        {
                            "identity": "00ceca763afc2c2e6933db580d4f788e4df7d6e8e6",
                            "key": "A/J83e6pRe9ARxCJNrUxu2iVfh3HTKk4CEYyZFxWn4NG"
                        }
                    ],
                    "withdrawals": [
                        {
                            "key": "0",
                            "value": {
                                "amount": "10000000000000000000",
                                "receiver": {
                                    "identifier": {
                                        "data": "q/PmEcHO3J48fQ25Wd9bhLasuEk="
                                    }
                                },
                                "signatures": [
                                    "00ad793889e73e5e1329229afecf533d7ec73922f176490ca4edc8b11d922dd4700910197a9a9612a499e5960cd4cc42f535818518eed26a0776c1eb89ff12ce1b",
                                    "00df24ae7efb01d5fcd89b7be3d2d0abccbdd69c2eead00f353445ff6e434d7ac74c7d0248b240053899022cee90fc4977b4c259d760208ac28795a789433c0fba",
                                    "0017d8f30843c1a6b293980fcf48d46d5a3a5c105b7ba35b5ac37718325128c1e52ece9127b26036c4acf2690894244997ab70fc6d03e06e1b030b84d64c607e84"
                                ]
                            }
                        },
                        {
                            "key": "1",
                            "value": {
                                "amount": "10000000000000000000",
                                "receiver": {
                                    "identifier": {
                                        "data": "q/PmEcHO3J48fQ25Wd9bhLasuEk="
                                    }
                                },
                                "signatures": [
                                    "01d8b84c0c7da95baa840da2348fea87cd570d07b924c6e990f024a1974e5251944056d3e8aa4a9cfe00f5861225eab7663a40bb7b49ebfa3987f815178ba12c03",
                                    "0130743eb8bf4fd979a9e037fa0dc695fee22536ddaece1fc399b3a1043b6100a37f625cf13a538a5d9392e971acaa8f42dc7737babc05fe30c5a6e02c27ea626f",
                                    "00190dba79347e9c70df7b2158041621b4316b14d2d88a7768267d8dd74d7f45ec526b5c6cbff211a51f105bf95f0ea446d575f52d84bafcb93cb2e7bbc5b4e38d"
                                ]
                            }
                        },
                        {
                            "key": "2",
                            "value": {
                                "amount": "10000000000000000000",
                                "receiver": {
                                    "identifier": {
                                        "data": "q/PmEcHO3J48fQ25Wd9bhLasuEk="
                                    }
                                },
                                "signatures": [
                                    "0178dc7356c5849e8dede459b7bb8d6d8d88334d012ed9a895378cf9a98fb2cf100fade6c2c8a9f187df112f4ae24f49a984aad6c9b4d8e6674d189d0bca52dcb2",
                                    "000ded7da153687978f99194d318db290b0a2f59a6b2e0035c0d946a9dea65c88b031677bc3bfea8ec37437d0a836290fb83825a02e1e1aa2b73410904a6eb06b8",
                                    "0154a712e4d4c6518d625a44e1682fea7ea3d9726d8981f754dd9af97074e91d3861584c8df397b71ed5d173fb24a85bd5a317d46823d5276d759f274a5a83bb0a"
                                ]
                            }
                        },
                        {
                            "key": "3",
                            "value": {
                                "amount": "10000000000000000000",
                                "receiver": {
                                    "identifier": {
                                        "data": "q/PmEcHO3J48fQ25Wd9bhLasuEk="
                                    }
                                },
                                "signatures": [
                                    "00eb451910a166a71d6ce6c221959a7e017a376d6dab30c82d311f216148025fd5674f9b3b935087158a08e00e98ae0b1b8190a20b8eefdd8ffd72f72be46858d6",
                                    "014d9d099e88ab99cf47e355c3c5235667664383a38197f9972999763bcc13729854d671d60c7fd449eff5e37e28ee35a680bfd4d4ffb45c2b5cca28962688bf80",
                                    "01c145d527915dbeeb53ec85f5173020f1ffcf6af721dea3b306a72bfa2fe1d8a72fb4460ecaaaf207ac984d16817de1b49a52fc519121faeb09799d4b5941d471"
                                ]
                            }
                        },
                        {
                            "key": "4",
                            "value": {
                                "amount": "10000000000000000000",
                                "receiver": {
                                    "identifier": {
                                        "data": "q/PmEcHO3J48fQ25Wd9bhLasuEk="
                                    }
                                },
                                "signatures": [
                                    "002c873da3b76c05e7530b12681b068726b640b2d4345431eadf7814d1770214394be92a50b16afe8114942c7eaba0e4fd022f06cd412bd2caf404b6344a8acd82",
                                    "00ac9cde2a7bc48caf6a408ab3d84114c3f2ee0b0f3f4caecc46cdd4e13dccbce359a1b2bdc52d610fcc72520705fc7d5aa39b2a73a26efb6de7285eb2eecff8f6",
                                    "01e3492e1a16b271a48c44d660f9dfd219bf48a4805b5287952f7625c71938d2ef6a553a540587de2ff1163fd6f9da61a43ddbcfc686353f183c5767144745d1f1"
                                ]
                            }
                        }
                    ]
                }
            },
            {
                "key": "1",
                "value": {
                    "disputes": [],
                    "oracles": [
                        {
                            "identity": "00b2e734b5d8da089318d0d2b076c19f59c450855a",
                            "key": "A54hWPDXwNXybDeR7++nlZdlTnorJGT1Kx7mwTR3ae9X"
                        },
                        {
                            "identity": "00ceca763afc2c2e6933db580d4f788e4df7d6e8e6",
                            "key": "A/J83e6pRe9ARxCJNrUxu2iVfh3HTKk4CEYyZFxWn4NG"
                        },
                        {
                            "identity": "00a79f6a95f769e404764095a48d9c830e63510b88",
                            "key": "AmThsZafkQKXdpGkBDGwtnIFXc8xFjiX2ZZDRCDmyV3J"
                        }
                    ],
                    "withdrawals": [
                        {
                            "key": "0",
                            "value": {
                                "amount": "10000000000000000",
                                "receiver": {
                                    "identifier": {
                                        "data": "q/PmEcHO3J48fQ25Wd9bhLasuEk="
                                    }
                                },
                                "signatures": [
                                    "01109f1f55aaed70057a458ad55ef8383318ed082473bb4683605ba0300747f00529f0873c3e3032170eb34590358632b195676087d3277229b118f5f99f8d9a24",
                                    "0055ec3ed47021b181a53a1e762dc174f72a40cb9ca6d355031560f1545af11bf64591cdcca7ad18223e46a32570f5c4132d9b02bc7148a5a6bfe61caab4225810",
                                    "000fa1dddccb737667b1e02887985d45cbff8c4be2fc65be526f9f8ebf928b8def6d4ba676037a35e73949404706f22e739b2dee77b0ffb67a3dfddfd6cb031819"
                                ]
                            }
                        }
                    ]
                }
            }
        ],
        "largeOracleContract": "04f1ab744630e57fb9cfcd42e6ccbf386977680014",
        "maximumWithdrawalPerEpoch": "50000000000000000000",
        "oracleNonce": "1",
        "symbol": "MPC_ROPSTEN",
        "withdrawMinimum": "10000000000000000",
        "withdrawNonce": "1",
        "withdrawalSum": "10000000000000000"
    }

    const nonce_2 = partisiaCrypto.contract_system.Byoc.byocEthereumWithdrawMerkleProof({ withdrawalNonce: 2, withdraws: withdrawState.epochs[0].value.withdrawals })
    expect(nonce_2.length).toBe(3)
    expect(nonce_2[0].toString('hex')).toBe('498c86bd79bb1ed935c392fc9f4fd2dc2319fa28a4314352070ee63bbfcae88d')
    expect(nonce_2[1].toString('hex')).toBe('330abf7ad910068e9444cd9caf54d55d9d580bac9c596386d1b9bb2c6bfb973e')
    expect(nonce_2[2].toString('hex')).toBe('eab51e739dd67b6d572ebb895dbfcedb8c47719200b9236a160e5c2d23049581')

    const nonce_4 = partisiaCrypto.contract_system.Byoc.byocEthereumWithdrawMerkleProof({ withdrawalNonce: 4, withdraws: withdrawState.epochs[0].value.withdrawals })
    expect(nonce_4.length).toBe(1)
    expect(nonce_4[0].toString('hex')).toBe('706d49dd9ea4ac60732255fc5e27aeb6175708bb3ae49ddeb403d2c372040c85')

})
