import { Payload_ByocTransfer } from "../src/main/abi_system"
import { serializeToBuffer } from "../src/main/structs"

it('byoc transfer', () => {
    const abi = Payload_ByocTransfer
    const obj = {
        invocationByte: 29,
        address: '00e72e44eab933faaf1fd4ce94bb57e08bff98a1ed',
        amount: 1000,
        symbol: 'TEST_COIN'
    }

    const payload = serializeToBuffer(obj, ...abi)
    expect(payload.toString('hex')).toBe('1d00e72e44eab933faaf1fd4ce94bb57e08bff98a1ed00000000000003e800000009544553545f434f494e')
})