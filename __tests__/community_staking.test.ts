import { Payload_CommunityStakingAcceptAmount, Payload_CommunityStakingReclaimAmount, Payload_CommunityStakingReduceAmount, Payload_CommunityStakingStakeAmount } from "../src/main/abi_system"
import { serializeToBuffer } from "../src/main/structs"


// summary
// Community member requested to delegate 90.0000 MPC
// Node accepts 70.0000 MPC was accepted
// Node actually only want to accept 60.0000 MPC so he reduces the accepted stakes with 10.0000
// Afterwards the Node will have 60.0000 accepted and 30.0000 pending stakes from that community member
// Community Member withdrew 25.0000 MPC from the delegation to decrease the delegated stakes to 65.0000
// Afterwards the Node will have 60.0000 accepted and 5.0000 pending stakes from that community member

it('stake to node', () => {
    // Delegate 90.0000 MPC from 00c46f56cd02c35543cf41c23cb24909b29539ead4 to 009dc1601d9a0d8ef4dca2a975dec56abca3b000f3: https://testnet.mpcexplorer.com/transaction/eb3840063c5c2bca7f94f60e6f54e8024a310aec002179e7d8c041610c50d670
    const abi = Payload_CommunityStakingStakeAmount
    const obj = {
        invocationByte: 24,
        nodeAddress: '009dc1601d9a0d8ef4dca2a975dec56abca3b000f3',
        amount: 900000,
    }

    const payload = serializeToBuffer(obj, ...abi)
    expect(payload.toString('hex')).toBe('18009dc1601d9a0d8ef4dca2a975dec56abca3b000f300000000000dbba0')
})

it('accept stake from node', () => {
    // 009dc1601d9a0d8ef4dca2a975dec56abca3b000f3 accepts 70.0000 stakes from 00c46f56cd02c35543cf41c23cb24909b29539ead4: https://testnet.mpcexplorer.com/transaction/c05da52e16c4e7d1e6178f572daf38996ea2bf07155f660bfe4eba22f9115a98
    const abi = Payload_CommunityStakingAcceptAmount
    const obj = {
        invocationByte: 27,
        address: '00c46f56cd02c35543cf41c23cb24909b29539ead4',
        amount: 700000,
    }

    const payload = serializeToBuffer(obj, ...abi)
    expect(payload.toString('hex')).toBe('1b00c46f56cd02c35543cf41c23cb24909b29539ead400000000000aae60')
})

it('return stakes', () => {
    // 009dc1601d9a0d8ef4dca2a975dec56abca3b000f3 reduce accepted stakes from 00c46f56cd02c35543cf41c23cb24909b29539ead4 with 10.0000: https://testnet.mpcexplorer.com/transaction/b76035bc8f2d21e67a9b45365ca0ac52d3f7f7f2a55670c04068ea62dabf2f27
    const abi = Payload_CommunityStakingReduceAmount
    const obj = {
        invocationByte: 28,
        address: '00c46f56cd02c35543cf41c23cb24909b29539ead4',
        amount: 100000,
    }

    const payload = serializeToBuffer(obj, ...abi)
    expect(payload.toString('hex')).toBe('1c00c46f56cd02c35543cf41c23cb24909b29539ead400000000000186a0')
})

it('user reclaim pending stakes', () => {
    // Retract 25.0000 MPC from 00c46f56cd02c35543cf41c23cb24909b29539ead4 to 009dc1601d9a0d8ef4dca2a975dec56abca3b000f3: https://testnet.mpcexplorer.com/transaction/dd9b841a815eb0358c1f407ccfaf0d77c0cc13df33983a0aa53f17f7b476fe6e
    const abi = Payload_CommunityStakingReclaimAmount
    const obj = {
        invocationByte: 25,
        nodeAddress: '009dc1601d9a0d8ef4dca2a975dec56abca3b000f3',
        amount: 250000,
    }

    const payload = serializeToBuffer(obj, ...abi)
    expect(payload.toString('hex')).toBe('19009dc1601d9a0d8ef4dca2a975dec56abca3b000f3000000000003d090')
})
