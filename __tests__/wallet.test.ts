import { partisiaCrypto } from '../src/index'

it('wallet', () => {
  // 24 word mnemonic
  const mnemonic = partisiaCrypto.wallet.generateMnemonic(24)
  const keyPair = partisiaCrypto.wallet.getKeyPairLegacy(mnemonic)

  // this is you private key pair
  const { address, privateKey, publicKey } = keyPair

  const address_legacy = partisiaCrypto.wallet.privateKeyToAccountAddress(privateKey)
  expect(address_legacy).toEqual(address)
})

it('Serialize Transaction', () => {
  // Serialize Transaction
  const abi = partisiaCrypto.abi_system.Payload_MpcTransferWithMemo
  const txSerialized = partisiaCrypto.transaction.serializedTransaction(
    {
      nonce: 1, //'<get from querying address on chain>'
      validTo: '2022-08-30T20:45:09.493Z',
      cost: '20000',
    },
    {
      contract: '01a4082d9d560749ecd0ffa1dcaaaee2c2cb25d881',
    },
    {
      invocationByte: 23,
      recipient: '001122331122331122331122331122331122331122',
      amount: 10000,
      memo: 'Sending MPC',
    },
    abi
  )

  expect(txSerialized.toString('hex')).toEqual(
    '000000000000000100000182f08031f50000000000004e2001a4082d9d560749ecd0ffa1dcaaaee2c2cb25d8810000002d1700112233112233112233112233112233112233112200000000000027100000000b53656e64696e67204d5043'
  )
})

it('Deserialize Transaction', () => {
  // Deserialize Transaction
  const txSerialized = Buffer.from(
    '000000000000000100000182f08031f50000000000004e2001a4082d9d560749ecd0ffa1dcaaaee2c2cb25d8810000002d1700112233112233112233112233112233112233112200000000000027100000000b53656e64696e67204d5043',
    'hex'
  )
  const tx = partisiaCrypto.transaction.deserializeTransactionPayload(txSerialized, false)
  expect(tx).toMatchObject({
    signature: '',
    inner: { nonce: '1', validTo: '2022-08-30T20:45:09.493Z', cost: '20000' },
    header: {
      contract: '01a4082d9d560749ecd0ffa1dcaaaee2c2cb25d881',
      payload_length: '45',
    },
    payload: '1700112233112233112233112233112233112233112200000000000027100000000b53656e64696e67204d5043',
    deserialized: {
      invocationByte: 23,
      recipient: '001122331122331122331122331122331122331122',
      amount: '10000',
      memo: 'Sending MPC',
    },
  })
})
