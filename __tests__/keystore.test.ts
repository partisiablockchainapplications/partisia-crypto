import { partisiaCrypto } from '../src/index'

it('test keystore', () => {
  const private_key = 'c7bbfab97265e6a42dddce6ee799067f97745b36dfaf805abb95c3419c8f5344'
  const regPassword = 'Welcome1'
  const keyStore = partisiaCrypto.keystore.generateKeyStore(private_key, regPassword, true)

  const key = partisiaCrypto.keystore.getPrivateKeyFromKeyStore(keyStore, regPassword)

  expect(key === private_key)
})
