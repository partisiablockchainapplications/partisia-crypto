// https://jestjs.io/docs/en/expect.html#toequalvalue
import { partisiaCrypto } from '../src/index'

it('transfer payload', () => {
})

// it('transfer payload', () => {
//   const tx = {
//     transactionPayload:
//       'AHTLwR2RwKxf54UMZmNEM1yo5KIzO1I2wacExovRB/MBBh39UcQhbXoxuFN+h4suPS6FvHe++TubP8SqPj8HueIAAAAAAAAAAwAAAX0uA+RlAAAAAAAAABQCAAAAAAAAAAAAAAAAAAAAAAAAsAgAAAA3AwAAAG8AarHSJ6kfY/sKLiV3dO0qF58zLyIAp+QVl2kdSkaxhxptgrMsmoMptWMAAAAAAAAAAQ==',
//     block: 'de54a1b63127a785838b36dae68d125db74dd45f35394e8d68b425391ebc49c6',
//     from: '006ab1d227a91f63fb0a2e257774ed2a179f332f22',
//     identifier: '6fb9cabb5ffe506f1a2ca97072f9991d19dd6bec64002c5f3ddf503709dc0d0e',
//     executionSucceeded: true,
//     events: [
//       {
//         identifier: '8b1c2d465a3d282a55e9903c7ac0d0cd9671a0ee9a4fc20b603c6bfe4ec13ebe',
//         destinationShard: 'Shard0',
//       },
//     ],
//     finalized: true,
//   }

//   const payload = tx.transactionPayload

//   const x = partisiaCrypto.transaction.deserializeTransactionPayload(payload, true)
//   expect(x).toStrictEqual({
//     signature:
//       '0074cbc11d91c0ac5fe7850c666344335ca8e4a2333b5236c1a704c68bd107f301061dfd51c4216d7a31b8537e878b2e3d2e85bc77bef93b9b3fc4aa3e3f07b9e2',
//     inner: { nonce: '3', validTo: '2021-11-17T13:09:06.789Z', cost: '20' },
//     header: {
//       contract: '02000000000000000000000000000000000000b008',
//       payload_length: '55',
//     },
//     payload: {
//       invocationType: 3,
//       symbol: '111',
//       from: '006ab1d227a91f63fb0a2e257774ed2a179f332f22',
//       to: '00a7e41597691d4a46b1871a6d82b32c9a8329b563',
//       amount: '1',
//     },
//   })
// })

// it('mint payload', () => {
//   const tx = {
//     transactionPayload:
//       'AC8S3Er/UiV87hNbymvNEuyJdcQx2Z9uThKXaTGGLFwkaIPK3CHE/YZ7PhSclP4Y23k7CRZm4JrxYJACifFexJQAAAAAAAAAAQAAAX0uASRcAAAAAAAAABQCAAAAAAAAAAAAAAAAAAAAAAAAsAgAAAAOAAAAAG8AAAAAAAAD6AA=',
//     block: '2e53fd87ac85d4fc91d3d2ddd5c76d5ad565245436cf2efd9025fa8483349fc1',
//     from: '006ab1d227a91f63fb0a2e257774ed2a179f332f22',
//     identifier: 'e38b5a51b3e1a952e8016c5c54895f88ddea8debf79a609a06b48720291f22b9',
//     executionSucceeded: true,
//     events: [{ identifier: '82150142bfb79b5e99a53b5a2338781b131167b47474b6822e9dafaf7c02d2fa', destinationShard: 'Shard0' }],
//     finalized: true,
//   }

//   const payload = tx.transactionPayload

//   const x = partisiaCrypto.transaction.deserializeTransactionPayload(payload, true)
//   expect(x).toStrictEqual({
//     signature:
//       '002f12dc4aff52257cee135bca6bcd12ec8975c431d99f6e4e12976931862c5c246883cadc21c4fd867b3e149c94fe18db793b091666e09af160900289f15ec494',
//     inner: { nonce: '1', validTo: '2021-11-17T13:06:06.556Z', cost: '20' },
//     header: {
//       contract: '02000000000000000000000000000000000000b008',
//       payload_length: '14',
//     },
//     payload: { invocationType: 0, symbol: '111', amount: '1000', paused: false },
//   })
// })

// it('unknown abi payload', () => {
//   const tx = {
//     transactionPayload:
//       'ATJUYuKNqUT3FA2KyRpjgdrDAjHo2fxDOi0rFqfndkjNMapr7BLSgrrbWngf5UDqIyGPIPw9RnPEEsrMpECR6mUAAAAAAAAARwAAAX0uAXLkAAAAAAAAAAAE/hfRAJNyyO06xbeQsy40k1nCx+kAAAAKAAAAAAAAAAvpbw==',
//     block: '54f721f985319e64d7424d3787914b33e7cc3cb6356c0a4981f2eb0576a866b8',
//     from: '0036997527f23a4ddc38e3c7a78a19dbdc30897438',
//     identifier: 'b06526dc682b79d8c5bbac8d74af852f77db70715b530edf8d6c68abb0edb8a0',
//     executionSucceeded: true,
//     events: [
//       {
//         identifier: '8712bdda5d3856f534daa3382f12a11fbc118d7d06501fdd914f3c78e40279ec',
//         destinationShard: 'Shard1',
//       },
//     ],
//     finalized: true,
//   }

//   const payload = tx.transactionPayload

//   const x = partisiaCrypto.transaction.deserializeTransactionPayload(payload, true)
//   expect(x).toStrictEqual({
//     signature:
//       '01325462e28da944f7140d8ac91a6381dac30231e8d9fc433a2d2b16a7e77648cd31aa6bec12d282badb5a781fe540ea23218f20fc3d4673c412cacca44091ea65',
//     inner: { nonce: '71', validTo: '2021-11-17T13:06:26.660Z', cost: '0' },
//     header: {
//       contract: '04fe17d1009372c8ed3ac5b790b32e349359c2c7e9',
//       payload_length: '10',
//     },
//     payload: '000000000000000be96f',
//   })
// })
