/* System Contracts */
// https://gitlab.com/privacyblockchain/demo/betanet/-/blob/master/integration-test/src/test/java/io/privacyblockchain/betanet/contracts/BetanetAddresses.java
import { IFields } from './abi_structs'

// Rostein ETH Deposit
// 045dbd4c13df987d7fb4450e54bcd94b34a80f2351
export const Payload_BYOC: IFields[] = [
  /* 
    A single byte header indicating that this is a deposit transaction: 00
    8 bytes - the nonce of the deposit on the Eth contract: 0000000000000001
    21 bytes -  the destination account on PBC: 006ab1d227a91f63fb0a2e257774ed2a179f332f
    32 bytes - the deposited amount: 00000000000000000000000000000000000000000000000000470de4df820000
  */
  { field_name: 'depositType', field_type: 'num8' },
  { field_name: 'nonceEth', field_type: 'num64' },
  { field_name: 'address', field_type: 'address' },
  { field_name: 'amount', field_type: 'num256' },
]

// 043b1822925da011657f9ab3d6ff02cf1e0bfe0146
export const Payload_Byoc_Withdraw: IFields[] = [
  { field_name: 'invocationByte', field_type: 'num8' },
  { field_name: 'ethAddress', field_type: 'eth_address' },
  { field_name: 'amount', field_type: 'num256' },
]
export const Payload_Byoc_Withdraw_Signer: IFields[] = [
  { field_name: 'invocationByte', field_type: 'num8' },
  { field_name: 'epoch_nonce', field_type: 'num64' },
  { field_name: 'withdraw_nonce', field_type: 'num64' },
  { field_name: 'signature', field_type: 'signature' },
]

export const Payload_NodeRegister: IFields[] = [
  { field_name: 'invocationByte', field_type: 'num8' },
  { field_name: 'name', field_type: 'string' },
  { field_name: 'website', field_type: 'string' },
  { field_name: 'streetAddress', field_type: 'string' },
  { field_name: 'publicKey', field_type: 'publicKey' },
  { field_name: 'publicKeyBls', field_type: 'publicKeyBls' },
  { field_name: 'jurisdictionEntity', field_type: 'num32' },
  { field_name: 'jurisdictionServer', field_type: 'num32' },
  { field_name: 'signatureBls', field_type: 'signatureBls' },
]

export const Payload_NodeRegisterSynaps: IFields[] = [
  { field_name: 'invocationByte', field_type: 'num8' },
  { field_name: 'rawRequest', field_type: 'string' },
  { field_name: 'signatureOnRawRequest', field_type: 'signature' },
  { field_name: 'website', field_type: 'string' },
  { field_name: 'producerServerJurisdiction', field_type: 'num32' },
  { field_name: 'producerPublicKey', field_type: 'publicKey' },
  { field_name: 'producerBlsKey', field_type: 'publicKeyBls' },
  { field_name: 'popSignature', field_type: 'signatureBls' },
]

export const Payload_NodeUpdate: IFields[] = [
  { field_name: 'invocationByte', field_type: 'num8' },
  { field_name: 'name', field_type: 'string' },
  { field_name: 'website', field_type: 'string' },
  { field_name: 'streetAddress', field_type: 'string' },
  { field_name: 'jurisdictionEntity', field_type: 'num32' },
  { field_name: 'jurisdictionServer', field_type: 'num32' },
]

export const Payload_OracleAllocate: IFields[] = [
  { field_name: 'invocationByte', field_type: 'num8' },
  { field_name: 'amount', field_type: 'num64' },
]

// Staking MPC Tokens
// 01a4082d9d560749ecd0ffa1dcaaaee2c2cb25d881
export const Payload_StakeTokens: IFields[] = [
  { field_name: 'invocationByte', field_type: 'num8' },
  { field_name: 'amount', field_type: 'num64' },
]
export const Payload_UnstakeTokens = Payload_StakeTokens

export const Payload_MpcRetry: IFields[] = [
  { field_name: 'invocationByte', field_type: 'num8' },
  { field_name: 'transfer_hash', field_type: 'address' },
]
export const Payload_Vested: IFields[] = [{ field_name: 'invocationByte', field_type: 'num8' }]
export const Payload_PendingUnstake: IFields[] = [{ field_name: 'invocationByte', field_type: 'num8' }]
export const Payload_MpcTransfer: IFields[] = [
  { field_name: 'invocationByte', field_type: 'num8' },
  { field_name: 'recipient', field_type: 'address' },
  { field_name: 'amount', field_type: 'num64' },
]
export const Payload_ZKRegister: IFields[] = [
  { field_name: 'invocationByte', field_type: 'num8' },
  { field_name: 'endpoint', field_type: 'string' },
]
export const Payload_MpcTransferWithNum: IFields[] = [
  { field_name: 'invocationByte', field_type: 'num8' },
  { field_name: 'recipient', field_type: 'address' },
  { field_name: 'amount', field_type: 'num64' },
  { field_name: 'numberTag', field_type: 'num64' },
]

export const Payload_MpcTransferWithMemo: IFields[] = [
  { field_name: 'invocationByte', field_type: 'num8' },
  { field_name: 'recipient', field_type: 'address' },
  { field_name: 'amount', field_type: 'num64' },
  { field_name: 'memo', field_type: 'string' },
]

export const Payload_CommunityStakingStakeAmount: IFields[] = [
  { field_name: 'invocationByte', field_type: 'num8' },
  { field_name: 'nodeAddress', field_type: 'address' },
  { field_name: 'amount', field_type: 'num64' },
]

export const Payload_CommunityStakingAcceptAmount: IFields[] = [
  { field_name: 'invocationByte', field_type: 'num8' },
  { field_name: 'address', field_type: 'address' },
  { field_name: 'amount', field_type: 'num64' },
]

export const Payload_CommunityStakingReduceAmount: IFields[] = [
  { field_name: 'invocationByte', field_type: 'num8' },
  { field_name: 'address', field_type: 'address' },
  { field_name: 'amount', field_type: 'num64' },
]

export const Payload_ByocTransfer: IFields[] = [
  { field_name: 'invocationByte', field_type: 'num8' },
  { field_name: 'address', field_type: 'address' },
  { field_name: 'amount', field_type: 'num64' },
  { field_name: 'symbol', field_type: 'string' },
]

export const Payload_BYOC_Transfer: IFields[] = [
  { field_name: 'invocationByte', field_type: 'num8' },
  { field_name: 'to', field_type: 'address' },
  { field_name: 'amount', field_type: 'num128' },
]
export const Payload_BYOC_Transfer_From: IFields[] = [
  { field_name: 'invocationByte', field_type: 'num8' },
  { field_name: 'from', field_type: 'address' },
  { field_name: 'to', field_type: 'address' },
  { field_name: 'amount', field_type: 'num128' },
]
export const Payload_BYOC_Approve: IFields[] = [
  { field_name: 'invocationByte', field_type: 'num8' },
  { field_name: 'spender', field_type: 'address' },
  { field_name: 'amount', field_type: 'num128' },
]
export const Payload_BYOC_Abort: IFields[] = [
  { field_name: 'invocationByte', field_type: 'num8' },
  { field_name: 'tx_hash', field_type: 'hash' },
]

export const Payload_CommunityStakingReclaimAmount: IFields[] = [
  { field_name: 'invocationByte', field_type: 'num8' },
  { field_name: 'nodeAddress', field_type: 'address' },
  { field_name: 'amount', field_type: 'num64' },
]

// Public deploy contract
// 0197a0e238e924025bad144aa0c4913e46308f9a4d
export const Payload_PublicDeployContract: IFields[] = [
  { field_name: 'invocationByte', field_type: 'num8' },
  { field_name: 'wasm', field_type: 'string_hex' },
  { field_name: 'abi', field_type: 'string_hex' },
  { field_name: 'init', field_type: 'string_hex' },
]
// BYOC MPC_DEMO Deposit Contract
// 015dbd4c13df987d7fb4450e54bcd94b34a80f2351
// 01fe17d1009372c8ed3ac5b790b32e349359c2c7e9 // FEE_DISTRIBUTION
// 013f03a608a36190d2395beae1f4df68b6983e03fd
export const Payload_Rpc: IFields[] = [{ field_name: 'RPC', field_type: 'raw' }]

// TaceInvocation
// 032caf9ffe1b01c2ff2d02b31fbe36bad4e61aea4d
// 03bc90fa00b747c986515147f630ce4d30d2f38c6a
// TODO
export const Payload_TraceCommitResultVariable: IFields[] = [{ field_name: 'RPC', field_type: 'raw' }]
export const Payload_TraceOnChainOutput: IFields[] = [{ field_name: 'RPC', field_type: 'raw' }]
export const Payload_TraceUnableToCalculate: IFields[] = [{ field_name: 'RPC', field_type: 'raw' }]
export const Payload_TraceConfirmInput: IFields[] = [{ field_name: 'RPC', field_type: 'raw' }]
export const Payload_TraceRejectInput: IFields[] = [{ field_name: 'RPC', field_type: 'raw' }]
export const Payload_TraceZKInput: IFields[] = [{ field_name: 'RPC', field_type: 'raw' }]
export const Payload_TraceZKInputOnChain: IFields[] = [{ field_name: 'RPC', field_type: 'raw' }]
export const Payload_TraceOpenUserVariables: IFields[] = [{ field_name: 'RPC', field_type: 'raw' }]
export const Payload_TraceOpenInvocation: IFields[] = [{ field_name: 'invocationType', field_type: 'num8' }]

// 0114592cd7abae5d02e87b632cd0affc3ad2f3314f // Public Deploy
// Real Deploy Contract
// 0125b109835ad086fd4ddbdc308000f150a59663eb
// 0183a64a50bd60f32005c936b1e0549e2e381ce395
export const Payload_ContractAbi: IFields[] = [{ field_name: 'RPC', field_type: 'raw' }]
