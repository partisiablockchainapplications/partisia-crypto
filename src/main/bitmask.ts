import { Big } from 'big.js'
import { numToBuffer } from './utils-buffer'

// Convert a bitmask number into an array of booleans
export const bitmaskIntegerToArray = (n: number | string): boolean[] => {
  Big.RM = Big.roundUp
  const b = Number(new Big(n).div(255).toFixed(0))

  const aryU8 = numToBuffer(n, b * 8)
  const aryBits: boolean[][] = []

  for (let idx = 0; idx < aryU8.length; idx++) {
    const u8 = aryU8[idx]
    const bits: boolean[] = []
    for (let i = 0; i < 8; i++) {
      bits.push((u8 & (1 << i)) === 0 ? false : true)
    }
    aryBits.push(bits)
  }

  // return aryBits as flattened ary
  return [].concat(...aryBits)
}

export const bitmaskArrayToInteger = (array: boolean[]): number => {
  return array.reduce((p, c, i) => p.plus(new Big(2).pow(i).times(Number(c))), new Big(0)).toNumber()
}
