import crypto from 'crypto' // This is node.js library https://nodejs.org/api/crypto.html
import assert from 'assert'

const optionsDefault = {
  hashName: 'sha256',
  hashLength: 32,
  macName: 'sha256',
  macLength: 32,
  curveName: 'secp256k1',
  symmetricCypherName: 'aes-128-ecb',
  iv: Buffer.from(new Uint8Array(0)),
  // iv: null,
  keyFormat: 'uncompressed',
  s1: Buffer.from(new Uint8Array(0)),
  s2: Buffer.from(new Uint8Array(0)),
}
// E
function symmetricEncrypt(cypherName, iv, key, plaintext) {
  const cipher = crypto.createCipheriv(cypherName, key, iv)
  const firstChunk = cipher.update(plaintext)
  const secondChunk = cipher.final()
  return Buffer.concat([firstChunk, secondChunk])
}

// E-1
function symmetricDecrypt(cypherName, iv, key, ciphertext) {
  const cipher = crypto.createDecipheriv(cypherName, key, iv)
  const firstChunk = cipher.update(ciphertext)
  const secondChunk = cipher.final()
  return Buffer.concat([firstChunk, secondChunk])
}
// KDF
function hashMessage(cypherName, message) {
  return crypto
    .createHash(cypherName)
    .update(message)
    .digest()
}

// MAC
function macMessage(cypherName, key, message) {
  return crypto
    .createHmac(cypherName, key)
    .update(message)
    .digest()
}
export const encrypt = (publicKey: Buffer | string, message: Buffer | string, options: any = {}) => {
  publicKey = typeof publicKey === 'string' ? Buffer.from(publicKey, 'hex') : publicKey
  message = typeof message === 'string' ? Buffer.from(message, 'utf8') : message

  options = { ...optionsDefault, ...options }

  const ecdh = crypto.createECDH(options.curveName)
  // R
  const R = Buffer.from(ecdh.generateKeys(null, options.keyFormat))
  // S
  const sharedSecret = ecdh.computeSecret(publicKey)

  // uses KDF to derive a symmetric encryption and a MAC keys:
  // Ke || Km = KDF(S || S1)
  const hash = hashMessage(options.hashName, Buffer.concat([sharedSecret, options.s1], sharedSecret.length + options.s1.length))
  // Ke
  const encryptionKey = hash.slice(0, hash.length / 2)
  // Km
  const macKey = hash.slice(hash.length / 2)

  // encrypts the message:
  // c = E(Ke; m);
  const cipherText = symmetricEncrypt(options.symmetricCypherName, options.iv, encryptionKey, message)

  // computes the tag of encrypted message and S2:
  // d = MAC(Km; c || S2)
  const tag = Buffer.from(
    macMessage(options.macName, macKey, Buffer.concat([cipherText, options.s2], cipherText.length + options.s2.length))
  )
  // outputs R || c || d
  return Buffer.concat([R, cipherText, tag])
}
function equalConstTime(b1: Buffer, b2: Buffer) {
  if (b1.length !== b2.length) {
    return false
  }
  let result = 0
  for (let i = 0; i < b1.length; i++) {
    result |= b1[i] ^ b2[i]
  }
  return result === 0
}

export const decrypt = (ecdh: crypto.ECDH, message: Buffer, options: any = {}) => {
  options = { ...optionsDefault, ...options }

  const publicKeyLength = ecdh.getPublicKey(null, options.keyFormat).length
  // R
  const R = message.slice(0, publicKeyLength)
  // c
  const cipherText = message.slice(publicKeyLength, message.length - options.macLength)
  // d
  const messageTag = message.slice(message.length - options.macLength)

  // S
  const sharedSecret = ecdh.computeSecret(R)

  // derives keys the same way as Alice did:
  // Ke || Km = KDF(S || S1)
  const hash = hashMessage(options.hashName, Buffer.concat([sharedSecret, options.s1], sharedSecret.length + options.s1.length))
  // Ke
  const encryptionKey = hash.slice(0, hash.length / 2)
  // Km
  const macKey = hash.slice(hash.length / 2)

  // uses MAC to check the tag
  const keyTag = macMessage(options.macName, macKey, Buffer.concat([cipherText, options.s2], cipherText.length + options.s2.length))

  // outputs failed if d != MAC(Km; c || S2);
  assert(equalConstTime(messageTag, keyTag), 'Bad MAC')

  // uses symmetric encryption scheme to decrypt the message
  // m = E-1(Ke; c)
  return symmetricDecrypt(options.symmetricCypherName, options.iv, encryptionKey, cipherText)
}
