export interface FileAbi {
  header: 'PBCABI'
  version: number
  abi: ContractAbi
}

export interface ContractAbi {
  shortnameLength: number
  types: StructTypeAbi[]
  init: FnAbi
  actions: FnAbi[]
  state: TypeSpec
}

export interface StructTypeAbi {
  name: string
  fields: FieldAbi[]
}

export interface FieldAbi {
  name: string
  type: TypeSpec
}

export interface FnAbi {
  name: string
  shortname: string // ADDITIONAL FOR FRONTEND
  arguments: ArgumentAbi[]
  len?: number
}

export interface ArgumentAbi {
  name: string
  type: TypeSpec
}

export type AbiValue = string | { [key: string]: AbiValue } | AbiValue[]

export type TypeSpec = SimpleTypeSpec | CompositeTypeSpec | StructTypeSpec

export type CompositeTypeSpec = VecTypeSpec | BTreeMapTypeSpec | BTreeSetTypeSpec | ByteArrayTypeSpec

export interface StructTypeSpec {
  typeIndex: TypeIndex.Struct
  customTypeIndex: number
}

export enum TypeIndex {
  Struct,
  u8,
  u16,
  u32,
  u64,
  u128,
  i8,
  i16,
  i32,
  i64,
  i128,
  String,
  bool,
  Address,
  Vec,
  BTreeMap,
  BTreeSet,
  ByteArray,
}

export type NumericTypeIndex = NumericTypeSpec['typeIndex']

export interface NumericTypeSpec {
  typeIndex:
    | TypeIndex.u8
    | TypeIndex.u16
    | TypeIndex.u32
    | TypeIndex.u64
    | TypeIndex.u128
    | TypeIndex.i8
    | TypeIndex.i16
    | TypeIndex.i32
    | TypeIndex.i64
    | TypeIndex.i128
}

export type SimpleTypeIndex = SimpleTypeSpec['typeIndex']

export interface SimpleTypeSpec {
  typeIndex: NumericTypeIndex | TypeIndex.String | TypeIndex.bool | TypeIndex.Address
}

export interface VecTypeSpec {
  typeIndex: TypeIndex.Vec
  valueType: TypeSpec
}

export interface BTreeMapTypeSpec {
  typeIndex: TypeIndex.BTreeMap
  keyType: TypeSpec
  valueType: TypeSpec
}

export interface BTreeSetTypeSpec {
  typeIndex: TypeIndex.BTreeSet
  valueType: TypeSpec
}

export interface ByteArrayTypeSpec {
  typeIndex: TypeIndex.ByteArray
  length: number
}

const NUMERIC_TYPE_NAMES: { [key in NumericTypeIndex]: string } = {
  [TypeIndex.u8]: 'u8',
  [TypeIndex.u16]: 'u16',
  [TypeIndex.u32]: 'u32',
  [TypeIndex.u64]: 'u64',
  [TypeIndex.u128]: 'u128',
  [TypeIndex.i8]: 'i8',
  [TypeIndex.i16]: 'i16',
  [TypeIndex.i32]: 'i32',
  [TypeIndex.i64]: 'i64',
  [TypeIndex.i128]: 'i128',
}

const SIMPLE_TYPE_NAMES: { [key in SimpleTypeIndex]: string } = {
  ...NUMERIC_TYPE_NAMES,
  [TypeIndex.String]: 'String',
  [TypeIndex.bool]: 'bool',
  [TypeIndex.Address]: 'Address',
}

// TODO[MM]: Consider having lambdas to get dynamic type names
export const TYPE_NAMES: { [key in TypeIndex]: string } = {
  ...SIMPLE_TYPE_NAMES,
  [TypeIndex.Struct]: 'Struct',
  [TypeIndex.Vec]: 'Vec',
  [TypeIndex.BTreeMap]: 'BTreeMap',
  [TypeIndex.BTreeSet]: 'BTreeSet',
  [TypeIndex.ByteArray]: 'ByteArray',
}
