import assert from 'assert'
import { Big } from 'big.js'
import crypto from 'crypto' // This is node.js library https://nodejs.org/api/crypto.html

export function randomBytes(numBytes?: number) {
  return crypto.randomBytes(numBytes || 256)
}

export function createHashRandom(numBytes?: number): Buffer {
  return createHashSha256(randomBytes(numBytes))
}

export function createHashSha256(data: Buffer | string): Buffer {
  let buf: Buffer
  if (typeof data == 'string') {
    buf = Buffer.from(data, 'hex')
  } else {
    buf = data
  }
  return crypto
    .createHash('sha256')
    .update(buf)
    .digest()
}
export function bufFromString(str: string): Buffer {
  const buf = Buffer.from(str, 'utf8')
  return Buffer.concat([numToBuffer(buf.length, 32).reverse(), buf])
}
export function bufferToString(buf: Buffer): { len: number; val: string } {
  const len = Number(bufferToNum(buf.slice(0, 4)))
  return { len: len + 4, val: buf.slice(4, 4 + len).toString('utf8') }
}
export function hashBuffers(buffers: Buffer[]): Buffer {
  const hash = crypto.createHash('sha256')

  for (const buffer of buffers) {
    hash.update(buffer)
  }

  return Buffer.from(hash.digest())
}
export function bufferToNum(buf: Buffer | Uint8Array, isLE: boolean = false): string {
  Big.PE = 1000
  let sum = new Big(0)
  let rightShift = buf.byteLength * 8 - 8
  for (let i = 0; i < buf.byteLength; i++) {
    const byte = isLE ? buf[buf.byteLength - i - 1] : buf[i]
    sum = sum.plus(new Big(2).pow(rightShift).times(byte))
    rightShift -= 8
  }

  return sum.toString()
}

export function numToBuffer(num: string | number, bytes: number): Buffer {
  Big.RM = Big.roundDown
  let res = new Big(num)
  // Max size that can fit is pow(2,bytes) - 1
  assert(res.lt(new Big(2).pow(bytes)), 'number too big')

  let leftShift = bytes - 8
  assert(bytes % 8 === 0, 'invalid byte number')
  const aryBuf: Uint8Array = new Uint8Array(bytes / 8)
  while (leftShift >= 0) {
    const shiftBits = new Big(2).pow(leftShift)
    const pos = Number(res.div(shiftBits).toFixed(0))
    aryBuf[leftShift / 8] = pos

    res = res.minus(shiftBits.times(pos))
    leftShift -= 8
  }

  return Buffer.from(aryBuf)
}
