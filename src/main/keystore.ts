import assert from 'assert'
import crypto from 'crypto'
import { sha3_512 } from 'js-sha3'
import zxcvbn from 'zxcvbn'

const cipherAlg = 'aes-256-ctr'
const kdfDklen = 1 << 5
const kdfC = 1 << 18

export interface IKeyStore {
  cipher: string
  iv: string
  salt: string
  mac: string
}

export function passwordStrength(password: string): boolean {
  try {
    // 8 characters length
    assert(password.length >= 8)

    let countNumber = 0
    let countUpper = 0

    const aryNum = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    // prettier-ignore
    const aryUpper = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
    for (let i = 0; i < password.length; i++) {
      const l = password.charAt(i)
      if (aryNum.includes(l)) {
        countNumber++
      }
      if (aryUpper.includes(l)) {
        countUpper++
      }
    }

    return countNumber > 0 && countUpper > 0
  } catch (error) {
    return false
  }
}

export function generateKeyStore(bufPrivateKey: Uint8Array | Buffer, password: string, skipAttack?: boolean): IKeyStore
export function generateKeyStore(wif: string, password: string, skipAttack?: boolean): IKeyStore
export function generateKeyStore(...args: any[]): IKeyStore {
  const [wif, password, skipAttack] = args
  const encodeType = 'hex'

  let bufPrivateKey = typeof wif === 'string' ? Buffer.from(wif, 'hex') : Buffer.from(wif)
  assert(bufPrivateKey.length == 32, 'Invalid Private Key length')
  if (!skipAttack) {
    assert(passwordStrength(password), 'Password must be at least 8 charaters with at least one number and uppercase letter')
    const attack = zxcvbn(password)
    assert(attack.score >= 3, 'Password too weak, please avoid using dictionary words as part of password')
  }

  const salt = crypto.randomBytes(32)
  const iv = crypto.randomBytes(16)

  const { cipherFinal, bufferValue } = cipherFromSaltIv(bufPrivateKey, password, salt, iv)

  return {
    cipher: cipherFinal.toString(encodeType),
    iv: iv.toString(encodeType),
    salt: salt.toString(encodeType),
    mac: sha3_512(bufferValue),
  }
}

export function cipherFromSaltIv(data: Buffer, password: string, salt: Buffer, iv: Buffer) {
  const derivedKey = crypto.pbkdf2Sync(Buffer.from(password, 'utf8'), salt, kdfC, kdfDklen, 'sha256')
  const cipher = crypto.createCipheriv(cipherAlg, derivedKey.subarray(0, 32), iv)
  assert(cipher, 'Unsupported cipher')

  const cipherFinal = Buffer.concat([cipher.update(data), cipher.final()])
  const bufferValue = Buffer.concat([derivedKey.subarray(16, 32), cipherFinal])
  return { cipherFinal, bufferValue }
}

export function getPrivateKeyFromKeyStore(keystore: string | IKeyStore, password: string, skipMacCheck: boolean = false): string {
  const encodeType = 'hex'
  const aryKeystore = Object.freeze([
    { name: 'cipher', len: 32 },
    { name: 'iv', len: 16 },
    { name: 'salt', len: 32 },
    { name: 'mac', len: 64 },
  ])
  const objKeystore = typeof keystore == 'object' ? keystore : JSON.parse(keystore)
  for (const key of aryKeystore) {
    const objKey: string = objKeystore[key.name]
    assert(objKey, `missing key ${key.name}`)
    assert(objKey.length >= key.len * 2, `key invalid ${key.name}`)
    objKeystore[key.name] = objKey.toLowerCase().substring(0, key.len * 2)
  }
  const { cipher, iv, salt, mac } = objKeystore
  const derivedKey = crypto.pbkdf2Sync(Buffer.from(password, 'utf8'), Buffer.from(salt, encodeType), kdfC, kdfDklen, 'sha256')
  const ciphertext = Buffer.from(cipher, encodeType)
  const bufferValue = Buffer.concat([derivedKey.subarray(16, 32), ciphertext])

  if (!skipMacCheck) {
    const macSha = sha3_512(bufferValue)
    assert(mac === macSha, 'Keystore mac check failed (sha3_512) - wrong password?')
  }

  const decipher = crypto.createDecipheriv(cipherAlg, derivedKey.subarray(0, 32), Buffer.from(iv, encodeType))
  const bufPrivateKey = Buffer.concat([decipher.update(ciphertext), decipher.final()])

  return bufPrivateKey.toString('hex')
}
