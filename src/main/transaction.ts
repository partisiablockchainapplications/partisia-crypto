import assert from 'assert'
import { ABI_TransactionInner, ABI_TransactionHeader, IStructTransactionInner, IFields, IStructHeader } from './abi_structs'
import {
  Payload_Rpc,
  Payload_ContractAbi,
  Payload_TraceOpenInvocation,
  Payload_TraceCommitResultVariable,
  Payload_TraceOnChainOutput,
  Payload_TraceUnableToCalculate,
  Payload_TraceConfirmInput,
  Payload_TraceRejectInput,
  Payload_TraceZKInput,
  Payload_TraceZKInputOnChain,
  Payload_TraceOpenUserVariables,
} from './abi_system'
import { serializeToBuffer, deserializeFromBuffer, fieldTypeToSize, deserializePayloadData } from './structs'
import { bufFromString, hashBuffers, numToBuffer } from './utils-buffer'

enum TransactionType {
  DEPLOY = 0,
  INTERACT = 1,
  REMOVE = 2,
}
enum TokenInvocation {
  CREATE,
  MINT,
  DESTROY,
  TRANSFER,
  PAUSE,
  UNPAUSE,
  NEW_GUARD,
  SPLIT,
}
enum TaceInvocation {
  COMMIT_RESULT_VARIABLE,
  ON_CHAIN_OUTPUT,
  UNABLE_TO_CALCULATE,
  CONFIRM_INPUT,
  REJECT_INPUT,
  ZERO_KNOWLEDGE_INPUT,
  ZERO_KNOWLEDGE_INPUT_ON_CHAIN,
  OPEN_USER_VARIABLES,
  OPEN_INVOCATION,
}
export const CONSTANTS = {
  Cost: 20,
  TokenInvocation,
  TaceInvocation,
  TransactionType,
}
export function deriveDigest(chainId: string, serializedTransaction: Buffer) {
  const digest = hashBuffers([serializedTransaction, bufFromString(chainId)])
  return digest
}

export function getTransactionPayloadData(serializedTransaction: Buffer, signature: Buffer) {
  // remove chainId buffer to get serialized trx
  assert(signature.length === 65)
  const transactionPayload = Buffer.concat([signature, serializedTransaction])
  return transactionPayload.toString('base64')
}

export const buildTransferTokenBuffers = (args: {
  from: string
  to: string
  amount: string | number
  symbol: string | number
  invocation: number
}) => {
  const bufType = numToBuffer(args.invocation, 8)
  const bufSymbol = numToBuffer(args.symbol, 32).reverse()
  const bufFrom = Buffer.from(args.from, 'hex')
  const bufTo = Buffer.from(args.to, 'hex')
  const bufAmt = numToBuffer(args.amount, 64).reverse()
  return Buffer.concat([bufType, bufSymbol, bufFrom, bufTo, bufAmt])
}

export function getStructInner(args: { nonce: number | string; cost?: number | string; validTo?: string | Date }) {
  // Add the defaults for cost and validTo
  const { nonce, cost, validTo } = {
    cost: CONSTANTS.Cost,
    validTo: Date.now() + 1000 * 30,
    ...args,
  }
  const st: IStructTransactionInner = {
    cost: String(cost),
    nonce: String(nonce),
    validTo: String(new Date(validTo).getTime()),
  }
  return st
}
export function getStructHeader(args: { contract: string; payload_length: number | string }) {
  // Add the defaults
  const st: IStructHeader = {
    contract: args.contract,
    payload_length: String(args.payload_length),
    // transactionType: args.transactionType,
  }
  return st
}

export function serializedTransaction(
  dataInner: {
    nonce: string | number
    cost?: string | number
    validTo?: string
  },
  dataHeader: {
    contract: string
  },
  dataPayload: Object,
  abi: IFields[]
): Buffer
export function serializedTransaction(
  dataInner: {
    nonce: string | number
    cost?: string | number
    validTo?: string
  },
  dataHeader: {
    contract: string
  },
  dataPayload: Buffer
): Buffer
export function serializedTransaction(...args: any[]): Buffer {
  const [dataInner, dataHeader] = args
  let bufPayload: Buffer
  if (Buffer.isBuffer(args[2])) {
    bufPayload = args[2]
  } else {
    const dataPayload = args[2]
    const abi = args[3]
    for (const field of abi) {
      assert(dataPayload.hasOwnProperty(field.field_name), `malformed payload, missing ${field.field_name}`)
    }
    bufPayload = serializeToBuffer(dataPayload, ...abi)
  }
  const bufInner = serializeToBuffer(getStructInner(dataInner), ...ABI_TransactionInner)
  const bufHeader = serializeToBuffer(
    getStructHeader({
      ...dataInner,
      ...dataHeader,
      payload_length: bufPayload.length,
      // transactionType: TransactionType.INTERACT,
    }),
    ...ABI_TransactionHeader
  )

  const serializedTransaction = Buffer.concat([bufInner, bufHeader, bufPayload])
  return serializedTransaction
}

export function getTrxHash(trxDigest: Buffer, signature: Buffer) {
  return hashBuffers([trxDigest, signature]).toString('hex')
}

export function deserializeInner(inner: Buffer): IStructTransactionInner {
  assert(inner.length === 24, `expected byte length of 24 but got ${inner.length}`)
  const obj = deserializeFromBuffer(inner, ...ABI_TransactionInner)
  // Max date in JS is 8640000000000000 but JAVA is BIGINT 922337203685477580
  const dateMax = 8640000000000000
  const validTo = new Date(Math.min(dateMax, Number(obj.validTo))).toISOString()
  return { ...obj, validTo }
}

export function deserializeHeader(header: Buffer): IStructHeader {
  assert(header.length === 25, `expected byte length of 25 but got ${header.length}`)
  return deserializeFromBuffer(header, ...ABI_TransactionHeader)
}
export function deserializePayload(payload: Buffer, ABI: IFields[]): any {
  return deserializeFromBuffer(payload, ...ABI)
}

// DEPRECATED
// for deserializing contract 02000000000000000000000000000000000000b008
// export function deserializeTransferPayload(payload_data: Buffer | string) {
//   const bufPayload = typeof payload_data === 'string' ? Buffer.from(payload_data, 'hex') : payload_data
//   const invocationType = bufPayload[0]
//   const ABI = getContractAbi('02000000000000000000000000000000000000b008', invocationType)

//   return deserializePayload(bufPayload, ABI)
// }
export function deserializeTransactionPayload(
  trx: Buffer | string,
  includesSignature: boolean = true
): {
  signature: string
  inner: IStructTransactionInner
  header: IStructHeader
  payload: string
  deserialized?: Object
} {
  const transactionPayload = typeof trx === 'string' ? Buffer.from(trx, 'base64') : trx
  // deserialize transaction
  let bufTrx = transactionPayload
  let signature = ''
  if (includesSignature) {
    signature = transactionPayload.slice(0, 65).toString('hex')
    bufTrx = transactionPayload.slice(65)
  }

  const lenInner = ABI_TransactionInner.map(t => fieldTypeToSize(t.field_type)).reduce((pv, cv, idx) => cv + pv, 0)
  const lenHeader = ABI_TransactionHeader.map(t => fieldTypeToSize(t.field_type)).reduce((pv, cv, idx) => cv + pv, lenInner)

  // starts at 74
  const bufInner = bufTrx.slice(0, lenInner / 8)
  const bufHeader = bufTrx.slice(lenInner / 8, lenHeader / 8)
  const bufPayload = bufTrx.slice(lenHeader / 8)

  const inner: IStructTransactionInner = deserializeInner(bufInner)
  const header: IStructHeader = deserializeHeader(bufHeader)

  // can the payload be deserialized
  const payload = bufPayload.toString('hex')
  const deserialized = deserializePayloadData(bufPayload, header.contract)

  // console.log('objInner', JSON.stringify(objInner, null, 2))
  // console.log('objHeader', JSON.stringify(objHeader, null, 2))
  // console.log('objPayload', JSON.stringify(objPayload, null, 2))
  return {
    signature,
    inner,
    header,
    payload,
    deserialized,
  }
}

// const ABI_GOVERNANCE = [
//   '02000000000000000000000000000000000000b008',
//   '015dbd4c13df987d7fb4450e54bcd94b34a80f2351',
//   '01fe17d1009372c8ed3ac5b790b32e349359c2c7e9',
// ]
export function getContractAbi(contractAddress: String, transactionType: number): IFields[] {
  // const abiGov = ABI_GOVERNANCE.find(a => a === contractAddress)
  // if (abiGov) {
  switch (contractAddress) {
    // TRACE INVOCATION
    case '03bc90fa00b747c986515147f630ce4d30d2f38c6a':
    case '032caf9ffe1b01c2ff2d02b31fbe36bad4e61aea4d': {
      switch (transactionType) {
        case CONSTANTS.TaceInvocation.OPEN_INVOCATION:
          return Payload_TraceOpenInvocation
        case CONSTANTS.TaceInvocation.COMMIT_RESULT_VARIABLE:
          return Payload_TraceCommitResultVariable
        case CONSTANTS.TaceInvocation.ON_CHAIN_OUTPUT:
          return Payload_TraceOnChainOutput
        case CONSTANTS.TaceInvocation.UNABLE_TO_CALCULATE:
          return Payload_TraceUnableToCalculate
        case CONSTANTS.TaceInvocation.CONFIRM_INPUT:
          return Payload_TraceConfirmInput
        case CONSTANTS.TaceInvocation.REJECT_INPUT:
          return Payload_TraceRejectInput
        case CONSTANTS.TaceInvocation.ZERO_KNOWLEDGE_INPUT:
          return Payload_TraceZKInput
        case CONSTANTS.TaceInvocation.ZERO_KNOWLEDGE_INPUT_ON_CHAIN:
          return Payload_TraceZKInputOnChain
        case CONSTANTS.TaceInvocation.OPEN_USER_VARIABLES:
          return Payload_TraceOpenUserVariables
      }
    }
    case '010e22fb997e9494556a556142fbfc6107d8972b52':
    case '013f03a608a36190d2395beae1f4df68b6983e03fd':
    case '015dbd4c13df987d7fb4450e54bcd94b34a80f2351':
    case '01fe17d1009372c8ed3ac5b790b32e349359c2c7e9': {
      return Payload_Rpc
    }

    case '0114592cd7abae5d02e87b632cd0affc3ad2f3314f':
    case '0183a64a50bd60f32005c936b1e0549e2e381ce395':
    case '0125b109835ad086fd4ddbdc308000f150a59663eb': {
      return Payload_ContractAbi
    }
  }
  // todo query the contract to get the ABI
  assert(false, 'invalid transaction type')
}
