import assert from 'assert'
import { BufferTypes, IFields } from './abi_structs'
import {
  Payload_BYOC,
  Payload_ByocTransfer,
  Payload_BYOC_Approve,
  Payload_BYOC_Transfer,
  Payload_BYOC_Transfer_From,
  Payload_Byoc_Withdraw,
  Payload_Byoc_Withdraw_Signer,
  Payload_CommunityStakingAcceptAmount,
  Payload_CommunityStakingReclaimAmount,
  Payload_CommunityStakingReduceAmount,
  Payload_CommunityStakingStakeAmount,
  Payload_MpcRetry,
  Payload_MpcTransfer,
  Payload_MpcTransferWithMemo,
  Payload_MpcTransferWithNum,
  Payload_NodeRegister,
  Payload_NodeRegisterSynaps,
  Payload_NodeUpdate,
  Payload_OracleAllocate,
  Payload_PendingUnstake,
  Payload_PublicDeployContract,
  Payload_StakeTokens,
  Payload_UnstakeTokens,
  Payload_Vested,
  Payload_ZKRegister,
  Payload_BYOC_Abort,
} from './abi_system'
import { bufferToNum, numToBuffer } from './utils-buffer'

// Size is in Bits
export function fieldTypeToSize(type: BufferTypes): number {
  if (type === 'address') {
    return (42 / 2) * 8
  }
  if (type === 'hash') {
    return (64 / 2) * 8
  }
  if (type === 'publicKey') {
    return 33 * 8
  }
  if (type === 'publicKeyBls') {
    return 96 * 8
  }
  if (type === 'eth_address') {
    return 20 * 8
  }
  if (type === 'signatureBls') {
    return 48 * 8
  }
  if (type === 'signature') {
    return 65 * 8
  }
  if (['string', 'string_hex'].includes(type)) {
    return 4 * 8 // the string length itself will be added later
  }

  if (type === 'num256') {
    return 256
  }
  if (type === 'num128') {
    return 128
  }
  if (type === 'num64') {
    return 64
  }
  if (type === 'num32') {
    return 32
  }
  if (type === 'num16') {
    return 16
  }
  if (type === 'num8') {
    return 8
  }
  if (type === 'bool') {
    return 8
  }

  assert('unkown field byte length')
}
export function serializeToBuffer(args: Object, ...aryFields: IFields[]) {
  // const bufSize = aryFields.map(f => fieldTypeToSize(f.field_type)).reduce((pv: number, cv: number) => { return cv + pv }, 0);
  const bufData: Uint8Array[] = []

  // Add in the payload length
  // const payloadLength = aryFields.filter(v => v.isPayload).reduce((pv, cv, idx) => pv + fieldTypeToSize(cv.field_type), 0)
  // const argsStruct = { ...args, payload_length: payloadLength / 8 }

  for (const field of aryFields) {
    const fieldVal = args[field.field_name]
    const bufSize = fieldTypeToSize(field.field_type)
    if (field.field_type.substring(0, 3) === 'num') {
      bufData.push(Buffer.from(numToBuffer(fieldVal, bufSize).reverse()))
    } else if (field.field_type === 'bool') {
      bufData.push(Buffer.from(numToBuffer(Number(fieldVal), bufSize)))
    } else if (['address', 'publicKey', 'publicKeyBls', 'signatureBls', 'hash', 'signature', 'eth_address'].includes(field.field_type)) {
      assert(typeof fieldVal === 'string', `pass ${field.field_type} as string`)
      let t = fieldVal.replace('0x', '')
      const bufSize = fieldTypeToSize(field.field_type)
      assert(t.length === bufSize / 4, `invalid ${field.field_type} ${fieldVal}`)
      bufData.push(Buffer.from(t, 'hex'))
    } else if (['string', 'string_hex'].includes(field.field_type)) {
      assert(typeof fieldVal === 'string')
      const bufStr = Buffer.from(fieldVal, field.field_type === 'string_hex' ? 'hex' : 'utf8')
      const bufLen = numToBuffer(bufStr.length, 32).reverse()
      bufData.push(Buffer.concat([bufLen, bufStr]))
    }
  }

  return Buffer.concat(bufData)
}

export function deserializeFromBuffer(buf: Buffer, ...aryFields: IFields[]): any {
  const obj = {}

  if (aryFields.length === 1 && aryFields[0].field_type === 'raw') {
    obj[aryFields[0].field_name] = buf.toString('hex')
    return obj
  }

  let intLen = 0
  for (const field of aryFields) {
    const lenStr = ['string', 'string_hex'].includes(field.field_type) ? Number(bufferToNum(buf.slice(intLen, intLen + 4))) : 0
    const bufSize = fieldTypeToSize(field.field_type)
    const byteLen = bufSize / 8
    assert(intLen + byteLen + lenStr <= buf.length, 'buffer is not large enough')
    const bufVal = buf.slice(intLen, byteLen + intLen + lenStr)
    intLen += byteLen + lenStr

    if (field.field_type.substring(0, 3) === 'num') {
      const num = bufferToNum(bufVal, false)
      // keep 32 and 64 as string; 8 and 16 can parse to number primitive
      if (['num8', 'num16'].includes(field.field_type)) {
        obj[field.field_name] = Number(num)
      } else {
        obj[field.field_name] = num
      }
    } else if (field.field_type === 'bool') {
      obj[field.field_name] = Boolean(Number(bufferToNum(bufVal)))
    } else if (['address', 'eth_address', 'publicKey', 'publicKeyBls', 'signatureBls', 'hash', 'signature'].includes(field.field_type)) {
      obj[field.field_name] = (field.field_type === 'eth_address' ? '0x' : '') + bufVal.toString('hex')
    } else if (['string', 'string_hex'].includes(field.field_type)) {
      if (field.field_type === 'string_hex') {
        obj[field.field_name] = bufVal.slice(4).toString('hex')
      } else {
        obj[field.field_name] = bufVal.slice(4).toString('utf8')
      }
    }
  }

  assert(intLen === buf.length, 'There is more buffer left that was not deserialized')
  return obj
}

export const getAbi = (headerContract: string) => {
  return SYSTEM_CONTRACTS.find(c => c.contract === headerContract)?.abi
}

export const deserializePayloadData = (payloadData: Buffer, contract: string): Object => {
  const aryAbi = getAbi(contract)
  if (!aryAbi) return null

  const enumInvocation = payloadData[0]
  if (aryAbi.length - 1 < enumInvocation) {
    return null
  }
  const abi = aryAbi[Number(enumInvocation)]
  if (!abi) return null
  const objPayload = deserializeFromBuffer(payloadData, ...abi)
  return objPayload
}
// List of known system contracts
// https://gitlab.com/privacyblockchain/demo/betanet/-/blob/main/src/main/java/com/partisiablockchain/server/BetanetAddresses.java#L44
// https://betareader.partisiablockchain.com/shards/Shard2/blockchain/contracts/<contract>\?requireContractState\=false
export const SYSTEM_CONTRACTS = [
  {
    contract: '04c5f00d7c6d70c3d0919fd7f81c7b9bfe16063620',
    shard_id: 0,
    name: 'System Update',
    abi: [],
  },
  {
    contract: '04f1ab744630e57fb9cfcd42e6ccbf386977680014',
    shard_id: 0,
    name: 'Large Oracle',
    abi: [
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      // 10 ASSOCIATE_TOKENS_TO_CONTRACT
      Payload_OracleAllocate,
      // 11 DISASSOCIATE_TOKENS_FROM_CONTRACT
      Payload_OracleAllocate,
    ],
  },
  {
    contract: '04fe17d1009372c8ed3ac5b790b32e349359c2c7e9',
    shard_id: 1,
    name: 'Fee Distribution',
    abi: [],
  },
  {
    contract: '045dbd4c13df987d7fb4450e54bcd94b34a80f2351',
    shard_id: 2,
    name: 'BYOC ETH Deposit',
    abi: [Payload_BYOC],
  },
  {
    contract: '043b1822925da011657f9ab3d6ff02cf1e0bfe0146',
    shard_id: 0,
    name: 'BYOC ETH Withdraw',
    abi: [Payload_Byoc_Withdraw, Payload_Byoc_Withdraw_Signer],
  },
  {
    contract: '042f2f190765e27f175424783a1a272e2a983ef372',
    shard_id: 0,
    name: 'BYOC Polygon Deposit',
    abi: [Payload_BYOC],
  },
  {
    contract: '04adfe4aaacc824657e49a59bdc8f14df87aa8531a',
    shard_id: 0,
    name: 'BYOC Polygon Withdraw',
    abi: [Payload_Byoc_Withdraw, Payload_Byoc_Withdraw_Signer],
  },
  {
    contract: '047e1c96cd53943d1e0712c48d022fb461140e6b9f',
    shard_id: 0,
    name: 'BYOC BNB Deposit',
    abi: [Payload_BYOC],
  },
  {
    contract: '044bd689e5fe2995d679e946a2046f69f022be7c10',
    shard_id: 0,
    name: 'BYOC BNB Withdraw',
    abi: [Payload_Byoc_Withdraw, Payload_Byoc_Withdraw_Signer],
  },
  {
    contract: '014aeb24bb43eb1d62c0cebf2a1318e63e35e53f96',
    shard_id: 0,
    name: 'Synaps Node Registration',
    abi: [undefined, Payload_NodeRegisterSynaps],
  },
  {
    contract: '04203b77743ad0ca831df9430a6be515195733ad91',
    shard_id: 0,
    name: 'Block Producer Orchestration',
    abi: [Payload_NodeRegister, undefined, undefined, undefined, undefined, Payload_NodeUpdate],
  },
  {
    // https://gitlab.com/secata/pbc/zk/real/zk-node-registry/-/blob/main/src/main/java/com/partisiablockchain/zk/real/ZkNodeRegistryContract.java#L86    contract: '01a4082d9d560749ecd0ffa1dcaaaee2c2cb25d881', shard_id: 1, name: 'MPC Token',
    contract: '01a2020bb33ef9e0323c7a3210d5cb7fd492aa0d65',
    shard_id: 0,
    name: 'ZK Node Registration',
    abi: [
      Payload_ZKRegister,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      // 10 ASSOCIATE_TOKENS_TO_CONTRACT
      Payload_StakeTokens,
      // 11 DISASSOCIATE_TOKENS_FROM_CONTRACT
      Payload_StakeTokens,
    ],
  },
  {
    // https://gitlab.com/partisiablockchain/governance/mpc-token/-/blob/main/src/main/java/com/partisiablockchain/governance/mpctoken/MpcTokenContract.java#L209
    contract: '01a4082d9d560749ecd0ffa1dcaaaee2c2cb25d881',
    shard_id: 1,
    name: 'MPC Token',
    abi: [
      Payload_StakeTokens,
      Payload_UnstakeTokens,
      undefined,
      // 3
      Payload_MpcTransfer,
      Payload_MpcRetry,
      Payload_PendingUnstake,
      // 6
      Payload_Vested,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      // 13
      Payload_MpcTransferWithNum,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      // 23
      Payload_MpcTransferWithMemo,
      // 24
      Payload_CommunityStakingStakeAmount,
      Payload_CommunityStakingReclaimAmount,
      undefined,
      Payload_CommunityStakingAcceptAmount,
      Payload_CommunityStakingReduceAmount,
      // 29
      Payload_ByocTransfer,
    ],
  },
  {
    contract: '0197a0e238e924025bad144aa0c4913e46308f9a4d',
    shard_id: 2,
    name: 'Public WASM Deploy',
    abi: [undefined, Payload_PublicDeployContract],
  },
  {
    contract: '014a6d0fd09fe2e6853a76caedcb46646ab7ee69d6',
    shard_id: 2,
    name: 'BYOC ETH',
    abi: [undefined, Payload_BYOC_Transfer, undefined, Payload_BYOC_Transfer_From, undefined, Payload_BYOC_Approve, Payload_BYOC_Abort],
  },
  {
    contract: '0137f4da8ad6a9a5305383953d4b3a9c7859c08bea',
    shard_id: 0,
    name: 'BYOC BNB',
    abi: [undefined, Payload_BYOC_Transfer, undefined, Payload_BYOC_Transfer_From, undefined, Payload_BYOC_Approve, Payload_BYOC_Abort],
  },
  {
    contract: '01e0dbf1ce62c4ebd76fa8aa81f3630e0e84001206',
    shard_id: 1,
    name: 'BYOC POLYGON_USDC',
    abi: [undefined, Payload_BYOC_Transfer, undefined, Payload_BYOC_Transfer_From, undefined, Payload_BYOC_Approve, Payload_BYOC_Abort],
  },
  {
    contract: '01f3cc99688e6141355c53752418230211facf063c',
    shard_id: 0,
    name: 'BYOC TEST_COIN',
    abi: [undefined, Payload_BYOC_Transfer, undefined, Payload_BYOC_Transfer_From, undefined, Payload_BYOC_Approve, Payload_BYOC_Abort],
  },
  {
    contract: '01dce90b5a0b6eb598dd6b4250f0f5924eb4a4a818',
    shard_id: 1,
    name: 'BYOC GOERLI ETH',
    abi: [undefined, Payload_BYOC_Transfer, undefined, Payload_BYOC_Transfer_From, undefined, Payload_BYOC_Approve, Payload_BYOC_Abort],
  },
]
