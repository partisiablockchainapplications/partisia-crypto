## Install

```bash
npm i partisia-blockchain-applications-crypto
```

---

## Wallet

### Wallet Mnemonic

```ts
import { partisiaCrypto } from 'partisia-blockchain-applications-crypto'

// 24 word mnemonic
const mnemonic = partisiaCrypto.wallet.generateMnemonic(24)
const keyPair = partisiaCrypto.wallet.getKeyPairHD(mnemonic)

// this is you private key pair
const { address, privateKey, publicKey } = keyPair
```

### Wallet Legacy

```ts
import { partisiaCrypto } from 'partisia-blockchain-applications-crypto'

// 24 word mnemonic
const mnemonic = partisiaCrypto.wallet.generateMnemonic(24)
const keyPair = partisiaCrypto.wallet.getKeyPairLegacy(mnemonic)

// this is you private key pair
const { address, privateKey, publicKey } = keyPair
```

### If you have a private key

```ts
const address = partisiaCrypto.wallet.privateKeyToAccountAddress('<32 bytes private key>')
```

---

## Transaction

### Serialize and Deserialize a Public Contract Transaction

> See: https://gitlab.com/partisiablockchain/language/abi-client-ts

### Serialize and Deserialize a System Contract Transaction

```ts
// Serialize Transaction
const abi = partisiaCrypto.abi_system.Payload_MpcTransferWithMemo
const txSerialized = partisiaCrypto.transaction.serializedTransaction(
  {
    nonce: 1, //'<get from querying address on chain>'
  },
  {
    contract: '01a4082d9d560749ecd0ffa1dcaaaee2c2cb25d881',
  },
  {
    invocationByte: 23,
    recipient: '001122331122331122331122331122331122331122',
    amount: 10000,
    memo: 'Sending MPC',
  },
  abi
)

console.log('txSerialized', txSerialized.toString('hex'))
// 000000000000000100000182f07ec3b6000000000000001401a4082d9d560749ecd0ffa1dcaaaee2c2cb25d8810000002d1700112233112233112233112233112233112233112200000000000027100000000b53656e64696e67204d5043

// Deserialize Transaction
const tx = partisiaCrypto.transaction.deserializeTransactionPayload(txSerialized, false)
console.log('tx', tx)

/* 
{
  signature: '',
  inner: { nonce: '1', validTo: '2022-08-30T20:45:09.493Z', cost: '20' },
  header: {
    contract: '01a4082d9d560749ecd0ffa1dcaaaee2c2cb25d881',
    payload_length: '45'
  },
  payload: '1700112233112233112233112233112233112233112200000000000027100000000b53656e64696e67204d5043',
  deserialized: {
    invocationByte: 23,
    recipient: '001122331122331122331122331122331122331122',
    amount: '10000',
    memo: 'Sending MPC'
  }
} 
*/
```

---

## Sign

### Sign Message

```ts
// Send message from Alice to Barry
// Alice encrypts message with Barry's public key
const message = 'hello'
const messageEncrypted = partisiaCrypto.wallet.encryptMessage('<public key of Barry>', message)

// Barry decrypts message with private key
const messageDecrypted = partisiaCrypto.wallet.decryptMessage('<private key>', messageEncrypted).toString('utf8')

assert(message === messageDecrypted)
```

### Sign Transaction

```ts
const digest = partisiaCrypto.transaction.deriveDigest(
  'Partisia Blockchain',
  Buffer.from(
    '000000000000000100000182f07ec3b6000000000000001401a4082d9d560749ecd0ffa1dcaaaee2c2cb25d8810000002d1700112233112233112233112233112233112233112200000000000027100000000b53656e64696e67204d5043',
    'hex'
  )
)
partisiaCrypto.wallet.signTransaction(digest, '<private key from wallet>')
```
